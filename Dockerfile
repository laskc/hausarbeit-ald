FROM debian:buster-slim

ENV SCRIPT="demo-word-accuracy.sh"

RUN apt-get update
RUN apt-get install --assume-yes build-essential time wget 

RUN mkdir /word2vec
COPY ./word2vec /word2vec
WORKDIR /word2vec

RUN chmod +x $SCRIPT

ENTRYPOINT [ "sh", "-c", "./${SCRIPT}"]