NAME = "Hausarbeit_ALD_Christian_Lask"
INTERMEDIATE_SUFFIX = $(shell date +%Y%m%d)

all: document

document: clean compile

clean:
	/bin/rm -f paper.aux paper.bbl paper.bcf paper.blg paper.ent paper.log paper.run.xml paper.toc
	/bin/rm -f *.pdf

compile:
	pdflatex paper.tex
	biber paper
	biber paper
	pdflatex paper.tex

export_intermediate: all
	mv paper.pdf ${NAME}_${INTERMEDIATE_SUFFIX}.pdf

export_final: all
	mv paper.pdf ${NAME}.pdf

word2vec: docker-build docker-run

docker-build:
	docker build --tag word2vec_ald:latest .

docker-run:
	docker run --tty --interactive --cpus=4 --memory=2048m word2vec_ald:latest